d = {'list1' : ['axa', 'xyz', 'gg', 'x', 'yyy'], 'list2' : ['x', 'cd', 'cnc', 'kk'], 'list3' : ['bab', 'ce', 'cba', 'syanora']}

for k, v in d.items():
	print('processing {}'.format(k))
	count = 0
	for i in v:
		if (len(i) == 3) and (i[0] == i[-1]):
			count +=1
	print("Count in {}: {}".format(k, count))
