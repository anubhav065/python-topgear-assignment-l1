list1 = [1, 2, 2, 3]
list2 = [2, 2, 3, 3, 3]

print("1st list is: {}".format(list1))
print("2nd list is: {}".format(list2))

print("Removing duplicate entries...\n")

print("1st list is: {}".format(list(set(list1))))
print("2nd list is: {}".format(list(set(list2))))